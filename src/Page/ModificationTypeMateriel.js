import React, {useState, useEffect, Fragment } from 'react';
import {useParams} from 'react-router-dom';
import { useToasts } from 'react-toast-notifications';


export default function ModificationTypeMateriel() {
  const [isBusy, setBusy] = useState(true);
  const { addToast } = useToasts();
    const [typeMateriel, setTypeMateriel] = useState([]);
    const [idTypeMateriel, setIdTypeMateriel] = useState('');
    const [libelleTypeMateriel, setLibelleTypeMateriel] = useState('');
    
    let{id} = useParams();

    const getTypeMateriel = () => {
      fetch(`${process.env.REACT_APP_URL}/typeMateriel/${id}`,{
        headers : { 
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }
      })
      .then(response => {
        return response.json();
      })
      .then(data => {
        setTypeMateriel(data);
        setIdTypeMateriel(data.id);
        setLibelleTypeMateriel(data.libelle);     
        setBusy(false);
      })
      .catch(error => {
        console.log(error);
      });
    }

    useEffect(() => {
      const fetchData = async () => {
        await getTypeMateriel();
      }
      fetchData()
    },[]);

  const changeHandlerId = e => {
    setIdTypeMateriel(e.target.value);
  };

  const changeHandlerLibelle = e => {
    setLibelleTypeMateriel(e.target.value);
  };




  
  function handleSubmit(e){
    e.preventDefault();
    
    fetch(`${process.env.REACT_APP_URL}/updateTypeMateriel`, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json; charset=UTF-8'},
      body: JSON.stringify({ 
          id: idTypeMateriel,
          libelle: libelleTypeMateriel})
    }).then(function(response){
      if(response.status === 200){
        addToast("modification réussite", { appearance: 'success', autoDismiss: true })
          window.location.href = "/ListerTypeMateriel";
      }
      });
  }
  return <Fragment>

{ isBusy ? (<div></div>) : (
    <div className="modification-type-salle" key={typeMateriel.id}>
        <div className="title-form">
            <h4>Modification type materiel</h4>
        </div>
      <form >
        <div className="form-row">
          <div className="form-group col-md-6">
            <label htmlFor="nom">Nom :</label>
            <input type="hidden" className="form-control" id="id" name = "id" value={idTypeMateriel}  onChange={changeHandlerId}/>
            <input type="text" className="form-control" id="libelle" name = "libelle" value={libelleTypeMateriel} onChange={changeHandlerLibelle}/>
          </div>
        </div>
        <button type="submit" className="btn btn-primary btn-form-create" onClick={handleSubmit}>
          Valider
        </button>
      </form>
    </div>
  ) }
  </Fragment>;
};

