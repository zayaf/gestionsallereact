import React, { useState, useEffect} from "react";
import { Link } from 'react-router-dom';
export default function ListerTypeSalle() {

    const [typeSalle, setTypeSalle] = useState([]);

    const getTypeSalle = () => {

        fetch(`${process.env.REACT_APP_URL}/typeSalles`, {
            headers : { 
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            }
             })
            .then(res => {
                return res.json();
            })
            .then(data => {
                setTypeSalle(data);
            })
            .catch(error => {
                console.log(error);
            })
    }
    useEffect(() => {
        
        const fetchData = async () => {
            await getTypeSalle();
        }
        fetchData()

    }, []);

    const typeSalleList = typeSalle.map((typeSalle) =>
        <tr key={typeSalle.id}>
            
            <td>{typeSalle.libelle}</td>
            <td><div className="btn-group" role="group">
									
                <Link to={`/ModificationTypeSalle/${typeSalle.id}`}><button className="btn btn-light" type="submit">Modifier</button></Link>
				<input  type="submit" className="btn btn-danger" name = "supprimer" value="supprimer" onClick={() => {deleteTypeSalle(typeSalle.id)}} />
				</div>
                </td>
        </tr>);
  

    return <div>
            <table className="table table-striped">
                <thead>
                    <tr>                       
                        <th scope="col">Nom</th>
                    </tr>
                </thead>
                <tbody>
                    {typeSalleList}
                </tbody>
            </table>
        </div>
}

function deleteTypeSalle(id) {
    fetch(`${process.env.REACT_APP_URL}/typeSalles/${id}`, {
       
        method : 'delete'
         })
        .then(res => {

            if(res.status === 200){

            }
            return res.text();
            
 
        })
        .catch(error => {
            console.log(error);
        })
        window.location.href = "/ListerTypeSalle";

}