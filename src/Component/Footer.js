import React from "react";
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";

const Footer = () => {
  return (
    <MDBFooter color="green" className="font-small pt-4 mt-4">
      <MDBContainer fluid className="text-center text-md-left">
        <MDBRow>
          <MDBCol md="6">
            
          </MDBCol>
          <MDBCol md="6">
            <h5 className="title">Visiter le site de l'AFPA</h5>
            <ul>
             
            </ul>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
      <div className="footer-copyright text-center py-3">
        <MDBContainer fluid>
          &copy; {new Date().getFullYear()} Copyright: <a href="cda-19158"> CDA-19158 </a>
        </MDBContainer>
      </div>
    </MDBFooter>
  );
}

export default Footer;