import React from "react";

export default class CreationUtilisateur extends React.Component {
  
  constructor(props){
    super(props);
    this.state = {
      type: '',
      nom: '',
      prenom: '',
      tel: '',
      email: '',
      adresse: '',
      role: {
        idRole: 0
      },
      authentification: {
        login: '',
        motDePasse: ''
      }
    }


  }

  
  handleUserInput (e) {
    const name = e.target.name;
    const value = e.target.value;
    if(name === "role"){
      if(value === "1"){
        this.setState({ type : "admin"});  
      }else if(value === "2"){
        this.setState({ type : "formateur"});  
      }else{
        this.setState({ type : "stagiaire"});  
      }
      this.setState({ role: {
            idRole : value
          }
        });      
    }
    
    else if(name === "login"){
      this.setState(
        prevState => ({
          authentification: {
            ...prevState.authentification,
            login : value
          }
        }));
    }else if(name === "mdp"){    
      this.setState(
        prevState => ({
          authentification: {
            ...prevState.authentification,
            motDePasse : value
          }
        }));
      }
    else{
      this.setState({[name]: value});  
      
    }
    console.log("id :" + this.state.role.idRole);
    console.log("type :" + this.state.type);
  }
  

  handleSubmit = (event) => {
    event.preventDefault();
    
    fetch(`${process.env.REACT_APP_URL}/personnes`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type':'application/json'},
      body: JSON.stringify(this.state),
    }).then(function(response){
      if(response.status === 200){
          alert('bien envoyé');
          window.location.href = "/ListerUtilisateur";
      }
      });
  }


  render() {
    return (
    <div className="bg">
      <div className="formulaire-creation">
        <div className="title-form">
          <h4>Formulaire de création d'utilisateur</h4>
        </div>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group-check">
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="radio" name="role" id="admin" value="1" checked={this.state.role.idRole === "1"}  onChange={(event) => this.handleUserInput(event)}/>
              <label className="form-check-label" htmlFor="admin">Admin</label>
            </div>
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="radio" name="role" id="formateur" value="2" checked={this.state.role.idRole === "2"} onChange={(event) => this.handleUserInput(event)}/>
              <label className="form-check-label" htmlFor="formateur">Formateur</label>
            </div>
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="radio" name="role" id="stagiaire" value="3" checked={this.state.role.idRole === "3"} onChange={(event) => this.handleUserInput(event)}/>
              <label className="form-check-label" htmlFor="stagiaire">Stagiaire </label>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-4">
              <label htmlFor="nom">Nom :</label>
              <input type="text" className="form-control" id="nom" required pattern="[A-Za-z ]{1,20}" name="nom" value={`${this.state.nom}`} onChange={(event) => this.handleUserInput(event)}/>
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="prenom">Prenom :</label>
              <input type="text" className="form-control" required pattern="[A-Za-z ]{1,20}" id="prenom"  name="prenom" value={`${this.state.prenom}`} onChange={(event) => this.handleUserInput(event)}/>
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="tel">Telephone :</label>
              <input type="tel" className="form-control" id="tel" required pattern="[0-9]{10}" name="tel" value={`${this.state.tel}`} onChange={(event) => this.handleUserInput(event)}/>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="email">Email</label>
              <input type="email" className="form-control" required  id="email" name="email" value={`${this.state.email}`} onChange={(event) => this.handleUserInput(event)}/>
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="adresse">Adresse</label>
              <input type="text" className="form-control" id="adresse" required name="adresse" value={`${this.state.adresse}`} onChange={(event) => this.handleUserInput(event)}/>
            </div>
          </div>

          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="login">Nom d'utilisateur</label>
              <input type="text" className="form-control" id="login" required name="login" value={`${this.state.authentification.login}`} onChange={(event) => this.handleUserInput(event)}/>
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="password">Mot de passe</label>
              <input type="password" className="form-control" required id="password" name="mdp" value={`${this.state.authentification.motDePasse}`}  onChange={(event) => this.handleUserInput(event)}/>
            </div>
          </div>

          <button type="submit" className="btn btn-primary btn-form-create">
            Valider
          </button>
        </form>
      </div>
    </div>
    )
  };
}
