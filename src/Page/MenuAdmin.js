import React from 'react';
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem, MDBNavLink, MDBDropdown, MDBDropdownToggle, MDBDropdownItem, MDBDropdownMenu  } from 'mdbreact';


class MenuAdmin extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        collapse: false,
      };
      this.onClick = this.onClick.bind(this);
    }
  
    onClick() {
      this.setState({
        collapse: !this.state.collapse,
      });
    }

    render() {
        return <div>
                <MDBNavbar className="navCss" dark expand="md">
                <MDBNavbarBrand href="/">
                        <strong>Gestion de Salle</strong>
                    </MDBNavbarBrand>
                    <MDBNavbarToggler onClick={this.onClick} />
                    <MDBCollapse isOpen={this.state.collapse} navbar>
                        <MDBNavbarNav left>
                        <MDBNavItem active>
                            <MDBNavLink to="#">Accueil</MDBNavLink>
                        </MDBNavItem>
                        <MDBNavItem>
                            <MDBNavLink to="#">A propos</MDBNavLink>
                        </MDBNavItem>
                        <MDBNavItem>
                            <MDBNavLink to="#">Le projet</MDBNavLink>
                        </MDBNavItem>
                        <MDBNavItem>
                            <MDBDropdown>
                                <MDBDropdownToggle nav caret>
                                <span className="mr-2">Option Admin</span>
                                </MDBDropdownToggle>
                                <MDBDropdownMenu>
                                <MDBDropdownItem href="/CreationUtilisateur">Créer un utilisateur</MDBDropdownItem>
                                <MDBDropdownItem href="#!">Lister les utilisateurs</MDBDropdownItem>
                                <MDBDropdownItem href="#!">Créer une salle</MDBDropdownItem>
                                <MDBDropdownItem href="#!">Lister les salles</MDBDropdownItem>
                                <MDBDropdownItem href="#!">Faire une reservation</MDBDropdownItem>
                                </MDBDropdownMenu>
                            </MDBDropdown>
                        </MDBNavItem>
                        </MDBNavbarNav>
                        <MDBNavbarNav right>
                        <MDBNavItem>
                            <MDBNavLink to="/Connect">Se déconnecter</MDBNavLink>
                        </MDBNavItem>
                      
                        </MDBNavbarNav>
                    </MDBCollapse>
                </MDBNavbar>
            </div>
    }
}

export default MenuAdmin

