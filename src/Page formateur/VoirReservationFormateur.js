import React, { useState, useEffect, Fragment } from 'react';
import { useParams, Link } from 'react-router-dom';


export default function VoirReservation() {
  const [reservation, setReservation] = useState({});
  const [isBusy, setBusy] = useState(true);
  let { id } = useParams();


  const getReservation = () => {
    
    fetch(`${process.env.REACT_APP_URL}/reservations/${id}`, {
        headers : { 
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }
    })
    .then(response => {
      return response.json();
    })
    .then(data => {
      
      setReservation(data);
      setBusy(false);
    })
    .catch(error => {
      console.log(error);
    });
  }

  useEffect(() => {
    const fetchData = async () => {
      
        await getReservation();
        
    }
    fetchData()
    
  }, []);

  const dateFormatMois = (mois) => {
      if(mois < 10){
        mois = "0"+mois;
      }
      return mois;
  }
  

  return <Fragment>
        <div className="container" key={reservation.idReservation}>
        { isBusy ? (<div>coucou</div>) : (
          <div>
            <p>Nom de la reservation : { reservation.motif } </p>
            <p>Reservé par : { reservation.personne.nom } { reservation.personne.prenom } le {dateFormatMois(reservation.dateDebut[2])}/{dateFormatMois(reservation.dateDebut[1])}/{reservation.dateDebut[0]} au {dateFormatMois(reservation.dateFin[2])}/{dateFormatMois(reservation.dateFin[1])}/{ reservation.dateFin[0]}</p>
            <p>La salle reservée est : { reservation.salle.nom } de type : { reservation.salle.type.libelle }</p>
            <p>La salle se situe dans le batiment { reservation.salle.batiment.nom }</p>
            <p>La salle contient :</p>
            <ul>
            
              {reservation.salle.listMateriel.map((materiel) =>
              <li className="list-group-item" key={materiel.id}>
                <span className="font-weight-bold">{materiel.nom}</span> : <span className="font-weight-bold">{materiel.quantite}</span><br />
              </li>)}       
            </ul>
            
              <Link to={`/ModificationReservation/${reservation.idReservation}`}><button className="btn btn-primary" type="submit">Modifier</button></Link>
        </div>
        )}
      </div>
      </Fragment>

  
    }

