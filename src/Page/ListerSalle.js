import React, { useState, useEffect } from "react";

import { Link } from 'react-router-dom';

export default function ListerSalles() {

    const [salles, setSalles] = useState([]);

    const getSalles = () => {

        fetch(`${process.env.REACT_APP_URL}/salles`, {
            headers : { 
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            }
             })

            .then(res => {
                return res.json();
            })

            .then(data => {

                setSalles(data);
                console.log(salles)
            })

            .catch(error => {
                console.log(error);

            })
    }
    useEffect(() => {
        
        const fetchData = async () => {
      
            await getSalles();
        }
        fetchData()
    }, []);

    const sallesList = salles.map((salle) =>
    
        <div className="col-sm-4" key={salle.idSalle}>
            <div className="card cardtop" >
            <img src={require('../img/typeSalle.jpg')} className="card-img-top" alt="..." />
                <div className="card-body">
                    <h5 className="card-title">Salle { salle.nom }</h5>
                    <Link to={`/VoirSalle/${salle.idSalle}`}><button className="btn btn-primary" type="submit">Voir</button></Link>
                    <input  type="submit" className="btn btn-danger" name = "supprimer" value="supprimer" onClick={() => {deleteSalle(salle.idSalle)}} />
                </div>
            </div>

        </div>);
    
    return <div className="bg">
           <div className="container">
           <div className="row">
                {sallesList}
                </div>
           </div>      
        </div>    
}

function deleteSalle(id) {
    fetch(`${process.env.REACT_APP_URL}/salles/${id}`, {
       
        method : 'delete'
         })
        .then(res => {

            if(res.status === 200){

            }
            return res.text();
            
 
        })
        .catch(error => {
            console.log(error);
        })
        window.location.href = "/ListerSalle";

}


