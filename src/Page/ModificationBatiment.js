import React, {useState, useEffect, Fragment } from 'react';
import { useToasts } from 'react-toast-notifications';
import {useParams} from 'react-router-dom';

export default function ModificationBatiment() {
  const { addToast } = useToasts();
  const [isBusy, setBusy] = useState(true);
    const [batiment, setBatiment] = useState([]);
    const [idBatiment, setIdBatiment] = useState('');
    const [nomBatiment, setNomBatiment] = useState('');

    let{id} = useParams();

    const getBatiment = () => {
      fetch(`${process.env.REACT_APP_URL}/batiments/${id}`,{
        headers : { 
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }
      })
      .then(response => {
        return response.json();
      })
      .then(data => {
        setBatiment(data);
        setIdBatiment(data.idBatiment);
        setNomBatiment(data.nom); 
        setBusy(false);

      })
      .catch(error => {
        console.log(error);
      });
    }

    useEffect(() => {
      const fetchData = async () => {
        await getBatiment();
      }
      fetchData()
    },[]);

    const changeHandlerId = e => {
      setIdBatiment(e.target.value);
    };
    
    const changeHandlerNom = e => {
      setNomBatiment(e.target.value);
    };

    
  function handleSubmit(e){
    e.preventDefault();
    
    fetch(`${process.env.REACT_APP_URL}/updateBatiments`, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json; charset=UTF-8'},
      body: JSON.stringify({ 
          idBatiment: idBatiment,
          nom: nomBatiment})
    }).then(function(response){
      if(response.status === 200){
        addToast("modification réussite", { appearance: 'success', autoDismiss: true })
          window.location.href = "/ListerBatiments";
      }
      });
  }

  return <Fragment>

  { isBusy ? (<div></div>) : (
  
    <div className="modification-batiment" key={batiment.idBatiment}>
        <div className="title-form">
            <h4>Modification du batiment</h4>
        </div>
      <form >
        <div className="form-row">
          <div className="form-group col-md-6">
            <label htmlFor="nom">Nouveau nom du batiment :</label>
            <input type="hidden" className="form-control" id="id" name = "id" value={idBatiment}  onChange={changeHandlerId}/>
            <input type="text" className="form-control" id="nom" name = "nom" value={nomBatiment} onChange={changeHandlerNom}/>
          </div>
        </div>
        <button type="submit" className="btn btn-primary btn-form-create"  onClick={handleSubmit}>
          Valider
        </button>
      </form>
    </div>
  ) }
  </Fragment>;
};

