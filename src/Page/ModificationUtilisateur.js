import React, { useState, useEffect, Fragment } from 'react';
import { useParams } from 'react-router-dom';
import { useToasts } from 'react-toast-notifications';

export default function ModificationUtilisateur() {
  const [isBusy, setBusy] = useState(true);
  const { addToast } = useToasts();
  const [personne, setPersonne] = useState([]);
  const [rolePersonne, setRolePersonne] = useState();
  const [libelleRole, setLibelleRole] = useState('');
  const [idPersonne, setIdpersonne] = useState('');
  const [nomPersonne, setNomPersonne] = useState('');
  const [prenomPersonne, setPrenomPersonne] = useState('');
  const [emailPersonne, setEmailPersonne] = useState('');
  const [telPersonne, setTelPersonne] = useState('');
  const [adrPersonne, setAdrPersonne] = useState('');
  const [loginPersonne, setLoginPersonne] = useState('');
  const [mdpPersonne, setMdpPersonne] = useState('');
  const [type, setType] = useState('');


  let { id } = useParams();

  const getUsers = () => {
    fetch(`${process.env.REACT_APP_URL}/personnes/${id}`, {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })

      .then(response => {
        return response.json();
      })
      .then(data => {
        setPersonne(data);
        setType(data.type);
        setIdpersonne(data.id);
        setRolePersonne(data.role.idRole);
        setLibelleRole(data.role.libelle);
        setNomPersonne(data.nom);
        setPrenomPersonne(data.prenom);
        setEmailPersonne(data.email);
        setTelPersonne(data.tel);
        setAdrPersonne(data.adresse);
        setLoginPersonne(data.authentification.login);
        setMdpPersonne(data.authentification.motDePasse);


        setBusy(false);

      })
      .catch(error => {
        console.log(error);
      });
  }

  useEffect(() => {
    const fetchData = async () => {
      await getUsers();
    }
    fetchData()
  }, []);


  const changeHandlerId = e => {
    setIdpersonne(e.target.value);
  };

  // à chaque modification du champ, il set les données du champ
  const changeHandlernom = e => {
    setNomPersonne(e.target.value);
  };

  const changeHandlerRole = e => {
    if (e.target.name === "role") {
      if (e.target.value === "1") {
        setType("admin");
        setLibelleRole("admin");
      } else if (e.target.value === "2") {
        setType("formateur");
        setLibelleRole("formateur");
      } else {
        setType("stagiaire");
        setLibelleRole("stagiaire");
      }

    }
    setRolePersonne(e.target.value);
  }



  const changeHandlerprenom = e => {
    setPrenomPersonne(e.target.value);
  };

  const changeHandlermail = e => {
    setEmailPersonne(e.target.value);
  };

  const changeHandlertel = e => {
    setTelPersonne(e.target.value);
  };

  const changeHandleradresse = e => {
    setAdrPersonne(e.target.value);
  };

  const changeHandlerlogin = e => {
    setLoginPersonne(e.target.value);
  };

  const changeHandlermdp = e => {
    setMdpPersonne(e.target.value);
  };


  function handleSubmit(e) {
    //il permet de ne pas recharger la page avant d'envoyer les données
    e.preventDefault();


    //pour envoyer les données 
    fetch(`${process.env.REACT_APP_URL}/updatePersonnes`, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json; charset=UTF-8'
      },
      // .stringgify pour transformer en string
      body: JSON.stringify({
        type: type,
        id: idPersonne,
       
        nom: nomPersonne,
        prenom: prenomPersonne,
        email: emailPersonne,
        tel: telPersonne,
        adresse: adrPersonne,
        role: {
          idRole: rolePersonne,
          libelle: libelleRole
        },
        authentification: {
          login : loginPersonne,
          motDePasse: mdpPersonne
      }})
    }).then(function (response) {
      //status 200 veut dire que tout s'est bien passé sur Postman
      if (response.status === 200) {
        addToast("modification réussite", { appearance: 'success', autoDismiss: true })
        window.location.href = "/ListerUtilisateur";
      }
    });

  }
//ajouter if pour récuperer le role après

    return <Fragment>
      <div className="modification-utilisateur" key={personne.id}>
        {isBusy ? (<div>Données en cours de téléchargement...</div>) : (
          <div>
            <div className="title-form">
              <h4>Modification utilisateur</h4>
            </div>
            <form >
              <div className="form-group-check">
                <div className="form-check form-check-inline">
                  <input type="hidden" className="idPersonne" id="id" name="id" value={idPersonne} onChange={changeHandlerId} />
                  <input className="form-check-input" type="radio" name="role" id="admin" value="1" checked={ rolePersonne === "1"} onChange={changeHandlerRole}   />
                  <label className="form-check-label" htmlFor="admin">Admin</label>
                </div>
                <div className="form-check form-check-inline">
                  <input className="form-check-input" type="radio" name="role" id="formateur" value="2" checked={ rolePersonne === "2"} onChange={changeHandlerRole} />
                  <label className="form-check-label" htmlFor="formateur">Formateur</label>
                </div>
                <div className="form-check form-check-inline">
                  <input className="form-check-input" type="radio" name="role" id="stagiaire" value="3" checked={ rolePersonne === "3"} onChange={changeHandlerRole} />
                  <label className="form-check-label" htmlFor="stagiaire">Stagiaire </label>
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md-6">
                  <label htmlFor="nom">Nom :</label>
                  <input type="text" className="form-control" id="nom" required pattern="[A-Za-z ]{1,20}" value={nomPersonne} onChange={changeHandlernom} />
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="prenom">Prenom :</label>
                  <input type="text" className="form-control" required pattern="[A-Za-z ]{1,20}" id="prenom" value={prenomPersonne} onChange={changeHandlerprenom} />
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md-6">
                  <label htmlFor="email">Email :</label>
                  <input type="email" className="form-control" required id="email" value={emailPersonne} onChange={changeHandlermail} />
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="telephone">Téléphone :</label>
                  <input type="tel" className="form-control" id="telephone" required value={telPersonne} pattern="[0][0-9]{9}" onChange={changeHandlertel} />
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="inputAdresse">Adresse :</label>
                  <input type="text" className="form-control" id="inputAdresse" required value={adrPersonne} onChange={changeHandleradresse} />
                </div>
              </div>

              <div className="form-row">
                <div className="form-group col-md-6">
                  <label htmlFor="login">Login :</label>
                  <input type="text" className="form-control" required id="login" value={loginPersonne} onChange={changeHandlerlogin} />
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="password">Mot de passe :</label>
                  <input type="password" className="form-control" required id="password" value={mdpPersonne} onChange={changeHandlermdp} />
                </div>
              </div>

              <button type="submit" className="btn btn-primary btn-form-create" onClick={handleSubmit}>
                Valider
        </button>
            </form>
          </div>
        )}
      </div>
    </Fragment>

};

