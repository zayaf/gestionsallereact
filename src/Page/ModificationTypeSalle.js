import React, {useState, useEffect, Fragment } from 'react';
import {useParams} from 'react-router-dom';
import { useToasts } from 'react-toast-notifications';


export default function ModificationTypeSalle() {
  const [isBusy, setBusy] = useState(true);

  //des variables pour stocker les données récupérées pour les afficher dans le formulaire
    const [typeSalle, setTypeSalle] = useState([]);
    const { addToast } = useToasts();
    const [idTypeSalle, setIdTypeSalle] = useState('');
    const [libelleTypeSalle, setLibelleTypeSalle] = useState('');
    
    let{id} = useParams();

    const getTypeSalle = () => {
      fetch(`${process.env.REACT_APP_URL}/typeSalles/${id}`,{
        headers : { 
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }
      })
      .then(response => {
        return response.json();
      })
      .then(data => {
       // setTypeSalle(data);
        setIdTypeSalle(data.id);
        setLibelleTypeSalle(data.libelle);     
        console.log(idTypeSalle);
        console.log(libelleTypeSalle);
        setBusy(false);
      })
      .catch(error => {
        console.log(error);
      });
    }

    useEffect(() => {
      const fetchData = async () => {
        await getTypeSalle();
      }
      fetchData()
    },[]);

  const changeHandlerId = e => {
    setIdTypeSalle(e.target.value);
  };

  // à chaque modification du champ, il set les données du champ
  const changeHandlerLibelle = e => {
    setLibelleTypeSalle(e.target.value);
  };




  
  function handleSubmit(e){
    //il permet de ne pas recharger la page avant d'envoyer les données
    e.preventDefault();
    
    //pour envoyer les données 
    fetch(`${process.env.REACT_APP_URL}/modifTypeSalle`, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json; charset=UTF-8'},
        // .stringgify pour transformer en string
      body: JSON.stringify({ 
          id: idTypeSalle,
          libelle: libelleTypeSalle})
    }).then(function(response){
      //status 200 veut dire que tout s'est bien passé sur Postman
      if(response.status === 200){
        addToast("modification réussite", { appearance: 'success', autoDismiss: true })
          window.location.href = "/ListerTypeSalle";      
      }
      });
  }
  return <Fragment>

{ isBusy ? (<div></div>) : (
    <div className="modification-type-salle" key={typeSalle.id}>
        <div className="title-form">
            <h4>Modification type salle</h4>
        </div>
      <form >
        <div className="form-row">
          <div className="form-group col-md-6">
            <label htmlFor="nom">Nom :</label>
            <input type="hidden" className="form-control" id="id" name = "id" value={idTypeSalle}  onChange={changeHandlerId}/>
            <input type="text" className="form-control" id="libelle" name = "libelle" value={libelleTypeSalle} onChange={changeHandlerLibelle}/>
          </div>
        </div>
        <button type="submit" className="btn btn-primary btn-form-create" onClick={handleSubmit}>
          Valider
        </button>
      </form>
    </div>
  ) }
  </Fragment>;
};

