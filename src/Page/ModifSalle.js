import React, { useState, useEffect, Fragment } from 'react';
import { useParams } from 'react-router-dom';
import { useToasts } from 'react-toast-notifications';

export default function ModifSalle() {
  
  const { addToast } = useToasts();
  const [isBusy, setBusy] = useState(true);
  const [salle, setSalle] = useState([]);
  const [typeSalle, setTypeSalle] = useState([]);
  const [idTypeSalle, setIdTypeSalle] = useState([]);
  const [materiels, setMateriels] = useState({})
  const [quantite, setQuantite] = useState();
  const [batiments, setBatiments] = useState([]);
  const [idBatiment, setIdBatiment] = useState([]);
  const [typeMateriel, setTypeMateriel] = useState([]);
  const [idTypeMateriel, setIdTypeMateriel] = useState('');
  const [libelleTypeMateriel, setLibelleTypeMateriel] = useState('');
  const [idSalle, setIdSalle] = useState();
  const [nomSalle, setNomSalle] = useState('');
  const [listeMaterielTest, setListeMaterielTest] = useState([]);


   

  let { id } = useParams();

  const getSalles = () => {
    fetch(`${process.env.REACT_APP_URL}/salles/${id}`, {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })

      .then(response => {
        return response.json();
      })
      .then(data => {
        setSalle(data);
        setIdSalle(data.idSalle);
        setNomSalle(data.nom);
        setListeMaterielTest(data.listeMaterielTest);
        
        //setTypeMateriel(data);
        //setIdTypeMateriel(data.idTypeMateriel);
     //   setQuantite(data.idTypeMateriel.quantite);
        //setLibelleTypeMateriel(data.libelleTypeMateriel);
        


        setBusy(false);

      })
      .catch(error => {
        console.log(error);
      });
  }

  useEffect(() => {
    const fetchData = async () => {
      await getSalles();
    }
    fetchData()
  }, []);

  const getTypeMateriel = () => {
    fetch(`${process.env.REACT_APP_URL}/typeMateriels`,{
      headers : { 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })
    .then(response => {
        return response.json();
    })
    .then(data => {
      setTypeMateriel(data);
      setIdTypeMateriel(data.idTypeMateriel);
      setLibelleTypeMateriel(data.libelleTypeMateriel);
     
      setBusy(false);

    })
    .catch(error => {
      console.log(error);
    });
  }

  const getBatiments = async () => {

    await fetch(`${process.env.REACT_APP_URL}/batiments`, {
        headers : { 
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }
         })
        .then(res => {
            return res.json();
        })
        .then(data => {
         
            setBatiments(data);
            setIdBatiment(data[0].idBatiment)
         
        })
        .catch(error => {
            console.log(error);
        })
       
}

useEffect(() => {
    const fetchData = async () => {
   //   await getSalles();
      await getBatiments();
      await getTypeMateriel();
      await getTypeSalle();
      
    }
    fetchData()
  },[]);

const getTypeSalle = () => {

  fetch(`${process.env.REACT_APP_URL}/typeSalles`, {
      headers : { 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
       })
      .then(res => {
          return res.json();
      })
      .then(data => {
          setTypeSalle(data);
          setIdTypeSalle(data[0].id)
      })
      .catch(error => {
          console.log(error);
      })
}

const changeHandlerId = e => {
    setIdSalle(e.target.value);
  };

  const changeHandlerNom= e => {
    setNomSalle(e.target.value);
   
  };
  
  const changeHandlerBatiment = e => {
    setIdBatiment(e.target.value);
   
  };

  const changeHandlerTypeSalle = e => {
    setIdTypeSalle(e.target.value);
   
  };

  const changeHandlerInputNumber = e => {
    
    setQuantite(e.target.value);
  }

  // à chaque modification du champ, il set les données du champ
  

  const handleSubmit = (event) => {
    event.preventDefault();
   
    let testMethode = document.getElementById("test").getElementsByTagName("input");
    let recup = [];
    for(let materiel of testMethode){
      recup.push({
        "id":materiel.id,
        "nom":materiel.name,
        "quantite":materiel.value
      })
    }
 
    


    event.preventDefault();
    
   fetch(`${process.env.REACT_APP_URL}/updateSalle`, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type':'application/json'},
      body: JSON.stringify({
          idSalle: idSalle,
        nom : nomSalle,
        type : {
          id: idTypeSalle
        },
        reserve : false,
        listMateriel : recup ,
        batiment: {
          idBatiment : idBatiment
        }
      })
    }).then(function(response){
      if(response.status === 200){
        addToast("modification réussite", { appearance: 'success', autoDismiss: true })
          window.location.href = "/ListerSalle";
      }
      });
  }


   
//ajouter if pour récuperer le role après

    return <Fragment>
     <div className="bg" key={salle.id} >
      <div className="formulaire-creation">
        <div className="title-form">
          <h4>Modification salle</h4>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="form-group-check">
            
          </div>
          <div className="form-row">
            <div className="form-group col-md-4">
              <label htmlFor="nom">Nom :</label>
              <input type="hidden" className="idPersonne"  id="id" name="id" value={idSalle} onChange={changeHandlerId} />
              <input type="text" className="form-control" id="nom" name="nom" value={nomSalle} onChange = {changeHandlerNom} />
              
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="selectBatiment">Sélectionner le batiment :</label> 
              <select className="form-control" id="selectBatiment" name="selectBatiment" onChange={changeHandlerBatiment}>
              {batiments.map((batiment) =>
            
                <option key = {batiment.idBatiment} value={batiment.idBatiment}>{batiment.nom}</option>
                )}
              </select>
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="selectTypeSalle">Sélectionner un type de salle :</label> 
              <select className="form-control" id="selectTypeSalle" name="selectTypeSalle"  onChange={changeHandlerTypeSalle}>
              {typeSalle.map((tSalle) =>{
                  if(salle.type.libelle === tSalle.libelle){
                  return <option selected key={tSalle.id} value ={tSalle.libelle}>{tSalle.libelle}</option>
                  }
                  else{
alert("else")
                      return <option key = {tSalle.id} value={tSalle.id}>{tSalle.libelle}</option>
                  }

              }

              )}
              </select>
            </div>
            
          </div>
          <div className="form-row" id="test">
            
            {typeMateriel.map((materiel) =>{
                    let mater = salle.listMateriel.find((mat) => {
                        return mat.nom == materiel.libelle;
                    })
                    {
                    return <div className="form-group col-md-6" key={materiel.id}>
                    <label htmlFor="nombreMateriel">{materiel.libelle}</label>
                    <input type="number" min ="0" className="form-control" id={mater.id} name={mater.nom} defaultValue={(mater.quantite)?mater.quantite:"0"} />

                  </div>   

                }
            }
        )}
        
          </div>
          
         

          <button type="submit" className="btn btn-primary btn-form-create">
            Valider
          </button>
        </form>
      </div>
    </div>
    </Fragment>

};

