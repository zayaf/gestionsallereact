import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
export default function ListerTypeMateriel() {

    const [typeMateriel, setTypeMateriel] = useState([]);

    const getTypeMateriel = () => {

        fetch(`${process.env.REACT_APP_URL}/typeMateriels`, {
            headers : { 
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            }
             })
            .then(res => {
                return res.json();
            })
            .then(data => {
                setTypeMateriel(data);
            })
            .catch(error => {
                console.log(error);
            })
    }
    useEffect(() => {
        
        const fetchData = async () => {
            await getTypeMateriel();
        }
        fetchData()

    }, []);

    const typeMaterielList = typeMateriel.map((typeMateriel) =>
        <tr key={typeMateriel.id}>
            
            <td>{typeMateriel.libelle}</td>
            <td><div className="btn-group" role="group">
									
                <Link to={`/ModificationTypeMateriel/${typeMateriel.id}`}><button className="btn btn-light" type="submit">Modifier</button></Link>

				<input  type="submit" className="btn btn-danger" name = "supprimer" value="supprimer" onClick={() => {deleteTypeMateriel(typeMateriel.id)}} />
				</div>
                </td>
        </tr>);
  

    return <div>
            <table className="table table-striped">
                <thead>
                    <tr>                       
                        <th scope="col">Nom</th>
                    </tr>
                </thead>
                <tbody>
                    {typeMaterielList}
                </tbody>
            </table>
        </div>
}

function deleteTypeMateriel(id) {
    fetch(`${process.env.REACT_APP_URL}/typeMateriel/${id}`, {
       
        method : 'delete'
         })
        .then(res => {

            if(res.status === 200){

            }
            return res.text();
            
 
        })
        .catch(error => {
            console.log(error);
        })
        window.location.href = "/ListerTypeMateriel";

}
