import React, { Component } from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBCard, MDBCardBody, MDBView, MDBMask, MDBCardTitle, MDBCardText, MDBBtn } from 'mdbreact';

class Body extends Component {
  render() {
    return (
        <div className="presentation">
        <div>
        <h1>Le projet en bref : 3 parties principales</h1>
        </div>
        <div class="card-deck">
        <div class="card">
          
          <div class="card-body">
            <h5 class="card-title">La gestion d'utilisateur</h5>
            <p class="card-text">Orientis vero limes in longum protentus et rectum ab Euphratis fluminis ripis ad usque supercilia porrigitur Nili, laeva Saracenis conterminans gentibus, dextra pelagi fragoribus patens, quam plagam Nicator Seleucus occupatam auxit magnum in modum, cum post Alexandri Macedonis obitum successorio iure teneret regna Persidis, efficaciae inpetrabilis rex, ut indicat cognomentum.</p>
          </div>
          <div class="card-footer">
            <small class="text-muted"></small>
          </div>
        </div>
        <div class="card">
          <img src="..." class="card-img-top" alt="..."/>
          <div class="card-body">
            <h5 class="card-title">La gestion de salle</h5>
            <p class="card-text">Orientis vero limes in longum protentus et rectum ab Euphratis fluminis ripis ad usque supercilia porrigitur Nili, laeva Saracenis conterminans gentibus, dextra pelagi fragoribus patens, quam plagam Nicator Seleucus occupatam auxit magnum in modum, cum post Alexandri Macedonis obitum successorio iure teneret regna Persidis, efficaciae inpetrabilis rex, ut indicat cognomentum.</p>
          </div>
          <div class="card-footer">
            <small class="text-muted"></small>
          </div>
        </div>
        <div class="card">
          <img src="..." class="card-img-top" alt="..."/>
          <div class="card-body">
            <h5 class="card-title">La gestion de reservation</h5>
            <p class="card-text">Orientis vero limes in longum protentus et rectum ab Euphratis fluminis ripis ad usque supercilia porrigitur Nili, laeva Saracenis conterminans gentibus, dextra pelagi fragoribus patens, quam plagam Nicator Seleucus occupatam auxit magnum in modum, cum post Alexandri Macedonis obitum successorio iure teneret regna Persidis, efficaciae inpetrabilis rex, ut indicat cognomentum.</p>
          </div>
          <div class="card-footer">
            <small class="text-muted"></small>
          </div>
        </div>
      </div>
      </div>
    );
  };
}

export default Body