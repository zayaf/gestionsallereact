import React, { useState, useEffect} from "react";
import { Link } from 'react-router-dom';
export default function ListerBatiments() {

    const [batiments, setBatiments] = useState([]);

    const getBatiments = () => {

        fetch(`${process.env.REACT_APP_URL}/batiments`, {
            headers : { 
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            }
             })
            .then(res => {
                return res.json();
            })
            .then(data => {
                setBatiments(data);
            })
            .catch(error => {
                console.log(error);
            })
    }
    useEffect(() => {
        
        const fetchData = async () => {
            await getBatiments();
        }
        fetchData()

    }, []);

    const batimentList = batiments.map((batiment) =>
        <tr key={batiment.idBatiment}>
            
            <td>{batiment.nom}</td>
            <td><div className="btn-group" role="group">
									
                <Link to={`/ModificationBatiment/${batiment.idBatiment}`}><button className="btn btn-light" type="submit">Modifier</button></Link>
				<input  type="submit" className="btn btn-danger" name = "supprimer" value="supprimer" onClick={() => {deleteBatiment(batiment.idBatiment)}} />
				</div>
                </td>
        </tr>);
  

    return <div>
            <table className="table table-striped">
                <thead>
                    <tr>                       
                        <th scope="col">Nom</th>
                    </tr>
                </thead>
                <tbody>
                    {batimentList}
                </tbody>
            </table>
        </div>
}

function deleteBatiment(id) {
    fetch(`${process.env.REACT_APP_URL}/batiments/${id}`, {
       
        method : 'delete'
         })
        .then(res => {

            if(res.status === 200){

            }
            return res.text();
            
 
        })
        .catch(error => {
            console.log(error);
        })
        window.location.href = "/ListerBatiments";

}