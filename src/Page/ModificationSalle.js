import React, {useState, useEffect, Fragment } from 'react';
import {useParams} from 'react-router-dom';
import { useToasts } from 'react-toast-notifications';

export default function ModificationSalle() {
    const [isBusy, setBusy] = useState(true);
    const { addToast } = useToasts();
      const [salle, setSalle] = useState([]);
      const [idSalle, setIdSalle] = useState('');
      const [nomSalle, setNomSalle] = useState('');
      const [typeSalle, setTypeSalle] = useState([]);
      const [idTypeSalle, setIdTypeSalle] = useState([]);
      const [quantite, setQuantite] = useState();
      const [batiments, setBatiments] = useState([]);
      const [idBatiment, setIdBatiment] = useState([]);
      const [typeMateriel, setTypeMateriel] = useState([]);
      const [idTypeMateriel, setIdTypeMateriel] = useState('');
      const [libelleTypeMateriel, setLibelleTypeMateriel] = useState('');

      let{id} = useParams();
  
      const getSalle = () => {
        fetch(`${process.env.REACT_APP_URL}/salles/${id}`,{
          headers : { 
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          }
        })
        .then(response => {
          return response.json();
        })
        .then(data => {
          setSalle(data);
          setIdSalle(data.idBatiment);
          setNomSalle(data.nom); 
          setBusy(false);
  
        })
        .catch(error => {
          console.log(error);
        });
      }
  
      useEffect(() => {
        const fetchData = async () => {
          await getSalle();
        }
        fetchData()
      },[]);
  
      const changeHandlerId = e => {
        setIdSalle(e.target.value);
      };
      
      const changeHandlerNom = e => {
        setNomSalle(e.target.value);
      };
  
      
    function handleSubmit(e){
      e.preventDefault();
      
      fetch(`${process.env.REACT_APP_URL}/updateSalle`, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-type': 'application/json; charset=UTF-8'},
        body: JSON.stringify({ 
            idSalle: idSalle,
            nom: nomSalle})
      }).then(function(response){
        if(response.status === 200){
          addToast("modification réussite", { appearance: 'success', autoDismiss: true })
            window.location.href = "/ListerSalle";
        }
        });
    }
  
    return <Fragment>
  
    { isBusy ? (<div></div>) : (
    
      <div className="modification-salle" key={salle.idSalle}>
          <div className="title-form">
              <h4>Modification de la salle</h4>
          </div>
        <form >
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="nom">Nom de la salle :</label>
              <input type="hidden" className="form-control" id="id" name = "id" value={idSalle}  onChange={changeHandlerId}/>
              <input type="text" className="form-control" id="nom" name = "nom" value={nomSalle} onChange={changeHandlerNom}/>
            </div>
          </div>
          <button type="submit" className="btn btn-primary btn-form-create"  onClick={handleSubmit}>
            Valider
          </button>
        </form>
      </div>
    ) }
    </Fragment>;
  };
  
  