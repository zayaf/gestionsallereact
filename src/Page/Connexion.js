import React, { useState } from "react";
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn } from 'mdbreact';

function FormPage (){

  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');
  const [isBusy, setBusy] = useState(true);


  async function submitForm(event){
    event.preventDefault();

    let response =  fetch(`${process.env.REACT_APP_URL}/authentification`, {
      method : "POST",
      body : JSON.stringify({
        login: login,
        motDePasse: password
      }),
      headers: {
        'Accept': 'application/json',
        "Content-type": "application/json; charset=UTF-8"
      }
  
    })
    .catch(error => console.log(error));

    sessionStorage.clear();
    if(response && response.status === 200){
      let token = await response.json();
      sessionStorage.setItem('token', token.token);
      alert('authentification reussi ');
      document.location.href= "/MenuAdmin";

    }else {
      if( response && response.status === 400) {
        alert('erreur authentification');
      } else{
        alert('serveur non disponible');
      }
    }
    
  };

 

  const changeHandlerLogin = e => {
    setLogin(e.target.value);
  };

  const changeHandlerPassword = e => {
    setPassword(e.target.value);

  };


return (
    <div class="bg">
<MDBContainer>
  <MDBRow>
    <MDBCol md="6" className="formConnexion">
      <form>
        <div className="logoFormConnect">
            <MDBRow>
                <MDBCol>
                    <img src={require('../img/logo2.jpg')} id="logo-afpa" className="img-fluid rounded-circle" alt="logo afpa" />
                </MDBCol>
            </MDBRow>
        </div>
        <p className="h5 text-center mb-4">CONNEXION</p>
        <div className="grey-text">
          <MDBInput label="Entrez votre login" icon="user" group type="login" validate error="wrong" success="right" name="login" value={login} onChange={changeHandlerLogin}/>
          <MDBInput label="Entrez votre mot de passe" icon="lock" group type="password" name = "password" value={password} onChange={changeHandlerPassword} validate />
        </div>
        <div className="text-center">
          <MDBBtn href="/MenuAdmin">Se connecter</MDBBtn>
        </div>
      </form>
    </MDBCol>
  </MDBRow>
</MDBContainer>
</div>
);
};

export default FormPage;