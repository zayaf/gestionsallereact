import React, { useState, useEffect } from "react";
import { createPortal } from "react-dom";

export default function CreationSalle() {
  const [isBusy, setBusy] = useState(true);
  const [typeSalle, setTypeSalle] = useState([]);
  const [idTypeSalle, setIdTypeSalle] = useState([]);
  const [materiels, setMateriels] = useState({})
  const [quantite, setQuantite] = useState();
  const [batiments, setBatiments] = useState([]);
  const [idBatiment, setIdBatiment] = useState([]);
  const [typeMateriel, setTypeMateriel] = useState([]);
  const [idTypeMateriel, setIdTypeMateriel] = useState('');
  const [libelleTypeMateriel, setLibelleTypeMateriel] = useState('');
  const [idSalle, setIdSalle] = useState('');
  const [nomSalle, setNomSalle] = useState('');


  const getTypeMateriel = () => {
    fetch(`${process.env.REACT_APP_URL}/typeMateriels`,{
      headers : { 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })
    .then(response => {
      return response.json();
    })
    .then(data => {
      setTypeMateriel(data);
      setIdTypeMateriel(data.idTypeMateriel);
      setLibelleTypeMateriel(data.libelleTypeMateriel);
      setBusy(false);

    })
    .catch(error => {
      console.log(error);
    });
  }

  const getBatiments = async () => {

    await fetch(`${process.env.REACT_APP_URL}/batiments`, {
        headers : { 
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }
         })
        .then(res => {
            return res.json();
        })
        .then(data => {
         
            setBatiments(data);
            setIdBatiment(data[0].idBatiment)
          
           

        })
        .catch(error => {
            console.log(error);
        })
       
}

const getTypeSalle = () => {

  fetch(`${process.env.REACT_APP_URL}/typeSalles`, {
      headers : { 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
       })
      .then(res => {
          return res.json();
      })
      .then(data => {
          setTypeSalle(data);
          setIdTypeSalle(data[0].id)
      })
      .catch(error => {
          console.log(error);
      })
}

  useEffect(() => {
    const fetchData = async () => {
      await getBatiments();
      await getTypeMateriel();
      await getTypeSalle();
    }
    fetchData()
  },[]);

  const changeHandlerNom= e => {
    setNomSalle(e.target.value);
   
  };
  
  const changeHandlerBatiment = e => {
    setIdBatiment(e.target.value);
   
  };

  const changeHandlerTypeSalle = e => {
    setIdTypeSalle(e.target.value);
   
  };

  const changeHandlerInputNumber = e => {
    
    setQuantite(e.target.value);
  }

  const handleSubmit = (event) => {
    event.preventDefault();
   
    let testMethode = document.getElementById("test").getElementsByTagName("input");
    let recup = [];
    for(let materiel of testMethode){
      recup.push({
        "id":materiel.id,
        "nom":materiel.name,
        "quantite":materiel.value
      })
    }
 
    //pour enlever la virgule 
    //recup = recup.substr(0,recup.length-1);
    event.preventDefault();
    
   fetch(`${process.env.REACT_APP_URL}/creationSalle`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type':'application/json'},
      body: JSON.stringify({
        nom : nomSalle,
        type : {
          id: idTypeSalle
        },
        reserve : false,
        listMateriel : recup ,
        batiment: {
          idBatiment : idBatiment
        }
      })
    }).then(function(response){
      if(response.status === 200){
          alert('bien envoyé');
          window.location.href = "/ListerSalle";
      }
      });
  }

/*body: JSON.stringify({
        nom : nomSalle,
        type : {
          id : idTypeSalle
        },
        reserve: false,
        listMateriel : [recup] ,
        batiment: {
          idBatiment: idBatiment
        }
      })*/

  return(
      <div className="bg">
      <div className="formulaire-creation">
        <div className="title-form">
          <h4>Creation salle</h4>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="form-group-check">
            
          </div>
          <div className="form-row">
            <div className="form-group col-md-4">
              <label htmlFor="nom">Nom :</label>
              <input type="text" className="form-control" id="nom" name="nom" value={nomSalle} onChange = {changeHandlerNom} />
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="selectBatiment">Sélectionner le batiment :</label> 
              <select className="form-control" id="selectBatiment" name="selectBatiment" onChange={changeHandlerBatiment}>
              {batiments.map((batiment) =>
            
                <option key = {batiment.idBatiment} value={batiment.idBatiment}>{batiment.nom}</option>
                )}
              </select>
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="selectTypeSalle">Sélectionner un type de salle :</label> 
              <select className="form-control" id="selectTypeSalle" name="selectTypeSalle" value = {quantite} onChange={changeHandlerTypeSalle}>
              {typeSalle.map((tSalle) =>
            
                <option key = {tSalle.id} value={tSalle.id}>{tSalle.libelle}</option>
                )}
              </select>
            </div>
            
          </div>
          <div className="form-row" id="test">
            
            {typeMateriel.map((materiel) =>
              <div className="form-group col-md-6" key={materiel.id}>
              <label htmlFor="nombreMateriel">{materiel.libelle}</label>
              <input type="number" min ="0" className="form-control" id={materiel.id} name={materiel.libelle} value = {quantite} />
           {/*   <input type="hidden" name={materiel.libelle} value={ materiel.libelle } />
			      	<input type="hidden" name={materiel.id} value={ materiel.id } />*/}
            </div>
        )}
        
          </div>
          
         

          <button type="submit" className="btn btn-primary btn-form-create">
            Valider
          </button>
        </form>
      </div>
    </div>
  )

}
