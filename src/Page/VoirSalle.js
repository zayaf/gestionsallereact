import React, { useState, useEffect, Fragment } from "react";
import { useParams, Link } from 'react-router-dom';


export default function VoirSalle() {
  const [isBusy, setBusy] = useState(true);
  const [salle, setSalle] = useState([]);
  let { id } = useParams();

  const getSalle = () => {
    fetch(`${process.env.REACT_APP_URL}/salles/${id}`, {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })

      .then(response => {
        return response.json();
      })
      .then(data => {
        setSalle(data);
        setBusy(false);

      })
      .catch(error => {
        console.log(error);
      });
  }

  useEffect(() => {
    const fetchData = async () => {
      await getSalle();
    }
    fetchData()
  }, []);
  return <Fragment>
    <div key={salle.idSalle}>
      {isBusy ? (<div></div>) : (
        <div className="voir-salle">
          <div className="title-form">
            <h4>Détails de la salle</h4>
          </div>
          <form >
            <div className="form-row">
              <div className="form-group col-md-6">
                <label htmlFor="nom">Nom :</label>
                <p>Salle {salle.nom}</p>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6">
                <label htmlFor="emplacement">Emplacement :</label>
                <p>Batiment {salle.batiment.nom}</p>
              </div>
              <div className="form-group col-md-6">
                <label htmlFor="typeSalle">Type de salle :</label>
                <p>{salle.type.libelle}</p>
              </div>
            </div>
            <label htmlFor="nom">Matériel(s) installé : </label>

            <div className="form-row">
              {salle.listMateriel.map((materiel) =>
                <div className="form-group col-md-6" key={materiel.id}>
                  <label htmlFor="quantite">{materiel.nom}</label>
                  <input type="number" min="0" className="form-control" id={materiel.id} name={materiel.libelle} defaultValue={materiel.quantite} />
                </div>
              )}

            </div>
            <div className="form-row">
              <div className="form-group col-md-6">
                <Link to={`/ModifSalle/${salle.idSalle}`}><button className="btn btn-light" type="submit">Modifier</button></Link>
              </div>
            </div>
          </form>
        </div>
      )}
    </div>
  </Fragment>
}