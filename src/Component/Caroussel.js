import React from "react";
import { MDBCarousel, MDBCarouselCaption, MDBCarouselInner, MDBCarouselItem, MDBView, MDBMask, MDBContainer } from
"mdbreact";
import Footer from "./Footer";
import Body from "./Body";

const Caroussel = () => {
  return (
      <div className="carouss">
          
    <MDBContainer>
      <MDBCarousel
      activeItem={1}
      length={3}
      showControls={true}
      showIndicators={true}
      className="z-depth-1"
    >
      <MDBCarouselInner>
        <MDBCarouselItem itemId="1">
          <MDBView>
            <img
              className="d-block w-100"
              src={require('../img/slideuser.jpg')}
              alt="First slide"
            />
          <MDBMask overlay="white-slight" />
          </MDBView>
          <MDBCarouselCaption>
            <h3 className="h3-responsive">Gestion d'utilisateur</h3>
            <p>Creer, Lister, Modifier, Supprimer utilisateur</p>
          </MDBCarouselCaption>
        </MDBCarouselItem>
        <MDBCarouselItem itemId="2">
          <MDBView>
            <img
              className="d-block w-100"
              src={require('../img/slidereserv.jpg')}
              alt="Second slide"
            />
          <MDBMask overlay="white-slight" />
          </MDBView>
          <MDBCarouselCaption>
            <h3 className="h3-responsive">Gestion reservation</h3>
            <p>Creer, Lister, Modifier, Supprimer reservation</p>
          </MDBCarouselCaption>
        </MDBCarouselItem>
        <MDBCarouselItem itemId="3">
          <MDBView>
            <img
              className="d-block w-100"
              src={require('../img/slidetypeSalle.jpg')}
              alt="Third slide"
            />
          <MDBMask overlay="white-slight" />
          </MDBView>
          <MDBCarouselCaption>
            <h3 className="h3-responsive">Gestion de salle</h3>
            <p>Creer, Lister, Modifier, Supprimer salle</p>
          </MDBCarouselCaption>
        </MDBCarouselItem>
      </MDBCarouselInner>
    </MDBCarousel>
    </MDBContainer>
    
    <div>
        <Body/>
    </div>
    <div>
       <Footer/>
    </div>
  
    </div>
  );
}

export default Caroussel;

