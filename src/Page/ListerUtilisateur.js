import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';

export default function ListerUtilisateur() {

    const [users, setUsers] = useState([]);

    const getUsers = () => {


        //lien à modifier
        fetch(`${process.env.REACT_APP_URL}/personnes`, {
            headers : { 
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            }
             })


            .then(res => {
                return res.json();
            })

            .then(data => {

                setUsers(data);
                
            })

            .catch(error => {
                console.log(error);

            })
    }
    useEffect(() => {
        
        const fetchData = async () => {
      
            await getUsers();
        }
        fetchData()

    }, []);

    const usersList = users.map((user) =>
        <tr key={user.id}>
            
            <td>{user.role.libelle}</td>
            <td>{user.nom}</td>
            <td>{user.prenom}</td>
            <td>{user.tel}</td>
            <td>{user.email}</td>
            <td>{user.adresse}</td>
            <td>{user.authentification.login}</td>
            <td>{user.authentification.motDePasse}</td>
            <td><div className="btn-group" role="group">
					
						
						
				<Link to={`/VoirUser/${user.id}`}><button className="btn btn-primary" type="submit">Voir</button></Link>
				
                <Link to={`/ModificationUtilisateur/${user.id}`}><button className="btn btn-light" type="submit">Modifier</button></Link>
				      	
				     
			      
			   {/*le () veut dire qu'on fait apelle à une fonction anonyme qui va executer la fonction deleteUser. Sinon elle allait supprimmer toute lla liste*/}
				      	
				      	<input  type="submit" className="btn btn-danger" name = "supprimer" value="supprimer" onClick={() => {deleteUser(user.id)}} />
			      
				</div></td>
        </tr>);
  

    return <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Role</th>
                        <th scope="col">Nom</th>
                        <th scope="col">Prenom</th>
                        <th scope="col">Tel</th>
                        <th scope="col">Mail</th>
                        <th scope="col">Adresse</th>
                        <th scope="col">Login</th>
                        <th scope="col">Mot de passe</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {usersList}
                </tbody>
            </table>
        </div>
}

function deleteUser(id) {
    //lien à modifier
    fetch(`${process.env.REACT_APP_URL}/personnes/${id}`, {
       
        method : 'delete'
         })
        .then(res => {

            if(res.status === 200){

            }
            return res.text();
            
 
        })
        .catch(error => {
            console.log(error);
        })
        window.location.href = "/ListerUtilisateur";

}