import React from 'react';

const menuAdmin = () => {

  return (
    <div>
      <div>
        <img src={require('../img/logo2.jpg')} id="logo-afpa" className="img-fluid rounded-circle" alt="logo afpa" />
      </div>

      <div className="card-deck">
        <div className="card">
          <img src={require('../img/bg1.jpg')} className="card-img-top" alt="..."/>
          <div className="card-body">
            <h5 className="card-title">Gestion salle</h5>

          </div>
          <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="CreationSalle">Creer salle</a>
          </nav>
          <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="ListerSalle">Lister les salles</a>
          </nav>


        </div>
        <div className="card">
          <img src={require('../img/batiment.jpg')} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">Gestion Batiment</h5>

          </div>
          <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="CreationBatiment">Creer batiment</a>
          </nav>
          <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="ListerBatiments">Lister Batiment</a>
          </nav>
        </div>
        <div className="card">
          <img src={require('../img/materiel.jpg')} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">Gestion Materiel</h5>

          </div>
          <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="CreationTypeMateriel">Creer type materiel</a>
          </nav>
          <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="ListerTypeMateriel">Lister type Materiel</a>
          </nav>
        </div>
        <div className="card">
          <img src={require('../img/user.jpg')} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">Gestion Utilisateur</h5>

          </div>
          <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="CreationUtilisateur">Creer Utilisateur</a>
          </nav>
          <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="ListerUtilisateur">Lister Utilisatuer</a>
          </nav>

        </div>
        <div className="card">
          <img src={require('../img/reservation.jpg')} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">Gestion Reservation</h5>

          </div>
          <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="CreationReservation">Creer Reservation</a>
          </nav>
          <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="ListerReservations">Lister Reservation</a>
          </nav>
        </div>

        <div className="card">
          <img src={require('../img/typeSalle.jpg')} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">Gestion type de salle</h5>

          </div>
          <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="CreationTypeSalle">Creer type de salle</a>
          </nav>
          <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="ListerTypeSalle">Lister type de salle</a>
          </nav>
        </div>

      </div>

    </div>


  )

}

export default menuAdmin;