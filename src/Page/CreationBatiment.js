import React from "react";

export default class CreationBatiment extends React.Component {
  
  constructor(props){
    super(props);
    this.state = { 
      nom: ''
    }


  }

  
  handleUserInput (e) {
    const name = e.target.name;
    const value = e.target.value;
   
    this.setState({[name]: value});  
   
  }
  

  handleSubmit = (event) => {
    event.preventDefault();
    
    fetch(`${process.env.REACT_APP_URL}/batiments`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type':'application/json'},
      body: JSON.stringify(this.state),
    }).then(function(response){
        if(response.status === 200){
            alert('bien envoyé');
            window.location.href = "/ListerBatiments";
        }
        });
        
  }

  render() {
    return (
    <div className="bg">
      <div className="formulaire-creation">
        <div className="title-form">
          <h4>Formulaire de création de batiment</h4>
        </div>
        <form onSubmit={this.handleSubmit}>
        
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="nom">Nom du batiment :</label>
              <input type="text" className="form-control" id="nom" name="nom" value={`${this.state.nom}`} onChange={(event) => this.handleUserInput(event)}/>
            </div>
            
          </div>
          <button type="submit" className="btn btn-primary btn-form-create">
            Valider
          </button>
        </form>
      </div>
    </div>
    )
  };
}
