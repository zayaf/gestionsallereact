import React, {useState, useEffect, Fragment } from "react";
import {useParams} from 'react-router-dom';
import { useToasts } from 'react-toast-notifications';

export default function CreationReservation() {
  const { addToast } = useToasts();
  const [reservation, setReservation] = useState([]);
  const [nomSalleReservation, setNomSalleReservation] = useState([]);
  const [idReservationR, setIdReservationR] = useState([]);
  const [motif, setMotif] = useState([]);
  const [salles, setSalles] = useState([]);
  const [dateDebutR, setDateDebutR] = useState({});
  const [dateFinR, setDateFinR] = useState({});
  const [idSalleR, setIdSalleR] = useState('');
  const [isBusy, setBusy] = useState(true);

  let{id} = useParams();

  const getFormateurs = () => {
    fetch(`${process.env.REACT_APP_URL}/reservations/${id}`, {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })
      .then(response => {
        return response.json();
      })
      .then(data => {
        setReservation(data);
        setIdReservationR(data.idReservation);
        setMotif(data.motif);
        setDateDebutR(convertDate(data.dateDebut[0], dateFormatMois(data.dateDebut[1]), dateFormatMois(data.dateDebut[2])));
        setDateFinR(convertDate(data.dateFin[0], dateFormatMois(data.dateFin[1]), dateFormatMois(data.dateFin[2])));
        setIdSalleR(data.salle.idSalle)
        setNomSalleReservation(data.salle.nom);
        setBusy(false);
      })
      .catch(error => {
        console.log(error);
      });
  }

  const getSalles = () => {
    fetch(`${process.env.REACT_APP_URL}/salles`, {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })
      .then(response => {
        return response.json();
      })
      .then(data => {
        setSalles(data);
        setBusy(false);
      })
      .catch(error => {
        console.log(error);
      });
  }

  useEffect(() => {
    const fetchData = async () => {
      await getFormateurs();
      await getSalles();
    }
    fetchData()
  }, []);

  const changeHandlerMotif = e => {
    setMotif(e.target.value);
  };

  // à chaque modification du champ, il set les données du champ
  const changeHandlerDateDebut = e => {
    setDateDebutR(e.target.value);
  };
  const changeHandlerDateFin = e => {
    setDateFinR(e.target.value);
  };

  const changeHandlerIDSalle = e => {
    setIdSalleR(e.target.value);
  };

  





  function handleSubmit(e) {
    //il permet de ne pas recharger la page avant d'envoyer les données
    e.preventDefault();

    //pour envoyer les données 
    if(compare()){
      fetch(`${process.env.REACT_APP_URL}/updateReservations`, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-type': 'application/json; charset=UTF-8'
        },
        // .stringgify pour transformer en string
        body: JSON.stringify({
          idReservation : idReservationR, 
          motif: motif,
          dateDebut: dateDebutR,
          dateFin: dateFinR,
          salle : {
            idSalle : idSalleR
          }
        })
      }).then(function (response) {
        //status 200 veut dire que tout s'est bien passé sur Postman
        if (response.status === 200) {
          addToast("modification réussite", { appearance: 'success', autoDismiss: true })
          window.location.href = "/ListerReservations";
        }
      });
    }
    else{
      alert('pas ok');
    }
    
  }

  const dateFormatMois = (mois) => {
    if(mois < 10){
      mois = "0"+mois;
    }
    return mois;
  }
  return <Fragment>

    {isBusy ? (<div></div>) : (
      <div>
        <form >

          <div className="form-group col-md-6">
            <label htmlFor="nom">Nom de la reservation</label>
            <input type="text" className="form-control" id="nom" placeholder="Nom de la reservation" name="nom" value={motif} onChange={changeHandlerMotif} required />
          </div>


          <div className="form-group col-md-6">
            <label htmlFor="startDate">Date de début :</label>
            <input type="date" className="form-control" id="startDate" name="dateReservationDebut" value={dateDebutR} onChange={changeHandlerDateDebut} required />
          </div>

          <div className="form-group col-md-6">
            <label htmlFor="endDate">Date de fin :</label>
            <input type="date" className="form-control" id="endDate" name="dateReservationFin"  value={dateFinR} onChange={changeHandlerDateFin} required />
          </div>

          <div className="form-group col-md-6">
            <label htmlFor="selectSalle">Quelle salle voulez-vous réserver ? </label> 
            <select className="form-control" id="selectSalle" name="selectSalle" onChange={changeHandlerIDSalle}>
             {salles.map((salle) =>  
                 {
                  if(salle.idSalle === idSalleR) {
                    return <option key = {salle.idSalle} value={salle.idSalle} selected>{salle.nom} - {salle.type.libelle}</option>
                  }else{
                    return <option key = {salle.idSalle} value={salle.idSalle}>{salle.nom} - {salle.type.libelle}</option>
                  }  
                 } 
                          
              )}
            </select>
          </div>

          <button type="submit" className="btn btn-primary" onClick={handleSubmit}>Valider</button>
        </form>
      </div>
    )}
  </Fragment>;

function compare()
{
    var startDt = document.getElementById("startDate").value;
    var endDt = document.getElementById("endDate").value;
    if( (new Date(startDt).getTime() < new Date(endDt).getTime()))
    {
        return true;
    }
  return false;
}




function convertDate(datey, datem, datej){
  return datey+"-"+datem+"-"+datej;

}

}