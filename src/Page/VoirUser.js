import React, {useState, useEffect, Fragment} from "react";
import { useParams } from 'react-router-dom';


export default function VoirUser() {
  const [isBusy, setBusy] = useState(true);
    const [personne, setPersonne] = useState([]);
    let{id} = useParams();

    const getUsers = () => {
      fetch(`${process.env.REACT_APP_URL}/personnes/${id}`,{
        headers : { 
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }
      })

      .then(response => {
        return response.json();
      })
      .then(data => {
        setPersonne(data);
        setBusy(false);

      })
      .catch(error => {
        console.log(error);
      });
    }

    useEffect(() => {
      const fetchData = async () => {
        await getUsers();
      }
      fetchData()
    },[]);
  
  return <Fragment> (
    <tr key = {personne.id}>
{isBusy ? (<div>coucou</div>) : (
    <div className="formulaire-creation">
        <div className="title-form">
            <h4>informations utilisateur</h4>
        </div>
      
        <div className="form-row">
          <div className="form-group col-md-4">
            <label for="nom">Nom :</label>
            <td>{personne.nom}</td>
          </div>
          <div className="form-group col-md-4">
            <label for="prenom">Prenom :</label>
            <td>{personne.prenom}</td>
          </div>
          <div className="form-group col-md-4">
            <label for="telephone">Telephone :</label>
            <td>{personne.tel}</td>
          </div>
        </div>
        <div className = "form-row">
            <div className="form-group col-md-6">
                <label for="email">Email</label>
              <td>{personne.email}</td>
            </div>
            <div className="form-group col-md-6">
            <label for="inputAddress">Address</label>
            <td>{personne.adresse}</td>
            </div>
        </div>
    
        <div className="form-row">
          <div className="form-group col-md-6">
            <label for="login">Nom d'utilisateur</label>
            <td>{personne.authentification.login}</td>
          </div>
          <div className="form-group col-md-6">
            <label for="password">Mot de passe</label>
            <td>{personne.authentification.motDePasse}</td>
          </div>
        </div></div>
        
        )}</tr>
  )
      </Fragment>
  }
