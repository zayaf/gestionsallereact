import React, { useState } from 'react';
import { Button } from 'mdbreact';

export default function Modal(props) {
    
    const [show, setShow] = useState(false);
  
    const handleClose = () => setShow(false);
    
  
    return (
      <>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Information :</Modal.Title>
          </Modal.Header>
          <Modal.Body>Votre a été ajouté </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
  