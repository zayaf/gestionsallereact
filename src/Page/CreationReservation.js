import React, {useState, useEffect, Fragment } from "react";


export default function CreationReservation() {

  const [formateurs, setFormateurs] = useState([]);
  const [salles, setSalles] = useState([]);
  const [reservation, setReservation] = useState('');
  const [dateDebutR, setDateDebutR] = useState([]);
  const [dateFinR, setDateFinR] = useState([]);
  const [idFormateur, setIdFormateur] = useState(0);
  const [idSalleR, setIdSalleR] = useState(0);
  const [isBusy, setBusy] = useState(true);


  const getFormateurs = () => {
    fetch(`${process.env.REACT_APP_URL}/formateur`, {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })
      .then(response => {
        return response.json();
      })
      .then(data => {
        setFormateurs(data);
        setIdFormateur(data[0].id);
        
        setBusy(false);
      })
      .catch(error => {
        console.log(error);
      });
  }

  const getSalles = () => {
    fetch(`${process.env.REACT_APP_URL}/salles`, {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })
      .then(response => {
        return response.json();
      })
      .then(data => {
        setSalles(data);
        setIdSalleR(data[0].idSalle);
        setBusy(false);
      })
      .catch(error => {
        console.log(error);
      });
  }

  useEffect(() => {
    const fetchData = async () => {
      await getFormateurs();
      await getSalles();
    }
    fetchData()
  }, []);

  const changeHandlerReservation = e => {
    setReservation(e.target.value);
  };

  // à chaque modification du champ, il set les données du champ
  const changeHandlerDateDebut = e => {
    setDateDebutR(e.target.value);
  };
  const changeHandlerDateFin = e => {
    setDateFinR(e.target.value);
  };

  const changeHandlerIDFormateur = e => {
    setIdFormateur(e.target.value);

  };

  const changeHandlerIDSalle = e => {
    setIdSalleR(e.target.value);

  };

  





  function handleSubmit(e) {
    //il permet de ne pas recharger la page avant d'envoyer les données
    e.preventDefault();

    if(compare()){
        //pour envoyer les données 
    fetch(`${process.env.REACT_APP_URL}/reservations`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json; charset=UTF-8'
      },
      // .stringgify pour transformer en string
      body: JSON.stringify({
        motif: reservation,
        dateDebut: dateDebutR,
        dateFin: dateFinR,
        personne : {
          type: "formateur",
          id: idFormateur
        },
        salle : {
          idSalle : idSalleR
        }
      })
    }).then(function (response) {
      //status 200 veut dire que tout s'est bien passé sur Postman
      if (response.status === 200) {
       alert('ok, bien envoyé');
        window.location.href = "/ListerReservations";
      }
    });
    }else{
      alert('pas ok');
    }
    
    
  }
  return <Fragment>

    {isBusy ? (<div></div>) : (
      <div>
        <form >

          <div className="form-group col-md-6">
            <label htmlFor="nom">Nom de la reservation</label>
            <input type="text" className="form-control" id="nom" placeholder="Nom de la reservation" name="nom" value={reservation} onChange={changeHandlerReservation} required />
          </div>


          <div className="form-group col-md-6">
            <label htmlFor="startDate">Date de début :</label>
            <input type="date" className="form-control" id="startDate" name="dateReservationDebut" value={dateDebutR} onChange={changeHandlerDateDebut} required />
          </div>

          <div className="form-group col-md-6">
            <label htmlFor="endDate">Date de fin :</label>
            <input type="date" className="form-control" id="endDate" name="dateReservationFin" value={dateFinR} onChange={changeHandlerDateFin} required />
          </div>

          <div className="form-group col-md-6">
            <label htmlFor="selectPersonne">Sélectionner le formateur qui souhaite réserver :</label> 
            <select className="form-control" id="selectPersonne" name="selectPersonne" value={idFormateur} onChange={changeHandlerIDFormateur}>
            {formateurs.map((formateur) =>
           
              <option key = {formateur.id} value={formateur.id}>{formateur.nom}</option>
              )}
            </select>
          </div>
          <div className="form-group col-md-6">
            <label htmlFor="selectSalle">Quelle salle voulez-vous réserver ? </label> 
            <select className="form-control" id="selectSalle" name="selectSalle" value={idSalleR} onChange={changeHandlerIDSalle}>
            {salles.map((salle) =>
           
              <option key = {salle.idSalle} value={salle.idSalle}>{salle.nom} - {salle.type.libelle}</option>
              )}
            </select>
          </div>

          <button type="submit" className="btn btn-primary" onClick={handleSubmit}>Valider</button>
        </form>
      </div>
    )}
  </Fragment>;

function compare()
{
    var startDt = document.getElementById("startDate").value;
    var endDt = document.getElementById("endDate").value;
    if( (new Date(startDt).getTime() < new Date(endDt).getTime()))
    {
        return true;
    }
  return false;
}
}

