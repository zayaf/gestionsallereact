import React from 'react';

import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem, MDBNavLink } from 'mdbreact';



class Menu extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        collapse: false,
        url : ''
      };
      this.onClick = this.onClick.bind(this);
      
    }
  
    onClick() {
      this.setState({
        collapse: !this.state.collapse,
      });
    }

    getUrl() {
      
        if(this.state.url==="MenuAdmin"){
            
            return <MDBNavLink to="/Connect">Se déconnecter</MDBNavLink>
            
        }else{
            
            return <MDBNavLink to="/Connect">Se connecter</MDBNavLink>
        }
    }

    render() {
        return <div>
            <div>
            <img
             
              src={require('../img/afpalogo.jpg')}
             
            />
            </div>
                <MDBNavbar className="navCss" dark expand="md">
                <MDBNavbarBrand href="/">
                        <strong>Gestion de Salle</strong>
                    </MDBNavbarBrand>
                    <MDBNavbarToggler onClick={this.onClick} />
                    <MDBCollapse isOpen={this.state.collapse} navbar>
                        <MDBNavbarNav left>
                        <MDBNavItem active>
                            <MDBNavLink to="/">Accueil</MDBNavLink>
                        </MDBNavItem>
                        <MDBNavItem>
                            <MDBNavLink to="#">A propos</MDBNavLink>
                        </MDBNavItem>
                        <MDBNavItem>
                            <MDBNavLink to="#">Le projet</MDBNavLink>
                        </MDBNavItem>
                        </MDBNavbarNav>
                        
                        <MDBNavbarNav right>
                        <MDBNavItem>
                            
                            { this.getUrl() }
                        </MDBNavItem>
                        </MDBNavbarNav>
                    </MDBCollapse>
                </MDBNavbar>
            </div>
    }
}

export default Menu

