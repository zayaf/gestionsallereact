import React, { Component } from "react";

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import FormPage from "./Page/Connexion";
import Menu from "./Component/Menu";
import { ToastProvider } from 'react-toast-notifications';
import MenuAdmin from "./Component/MenuAdminData";
import Accueil from "./Page/Accueil";
import CreationUtilisateur from "./Page/CreationUtilisateur";
import CreationBatiment from "./Page/CreationBatiment";
import CreationTypeMateriel from "./Page/CreationTypeMateriel";
import CreationTypeSalle from "./Page/CreationTypeSalle";
import ModificationReservation from "./Page/ModificationReservation";
import ListerReservations from "./Page/ListerReservations";
import ListerUtilisateur from "./Page/ListerUtilisateur";
import ListerBatiments from "./Page/ListerBatiments";
import VoirReservation from "./Page/VoirReservation";
import ModificationUtilisateur from "./Page/ModificationUtilisateur";
import ModificationTypeSalle from "./Page/ModificationTypeSalle";
import ModificationBatiment from "./Page/ModificationBatiment";
import ModificationTypeMateriel from "./Page/ModificationTypeMateriel";
import CreationSalle from "./Page/CreationSalle";
import VoirUser from "./Page/VoirUser";
import PageAdmin from "./Page/PageAdmin";
import ListerTypeSalle from "./Page/ListerTypeSalle";
import ListerTypeMateriel from "./Page/ListerTypeMateriel";
import ListerSalle from "./Page/ListerSalle";
import VoirSalle from "./Page/VoirSalle";
import ModificationSalle from "./Page/ModificationSalle";
import ModifSalle from "./Page/ModifSalle";
import "./index.css";
import "./App.css";
import CreationReservation from "./Page/CreationReservation";








class App extends Component {
 

  render() {
    
    return (
      <ToastProvider>
      <div>
       
        <Router>
      
        <header>
         
          <Menu />
        </header>
        
          <Switch>
        
          <Route path="/About">
            
          </Route>
          <Route path="/Project">
           
          </Route>
          <Route path="/Connect">
          
           
           <FormPage />
          </Route>
          <Route path="/MenuAdmin"> 
            
            <MenuAdmin />
          </Route>
          <Route path="/CreationUtilisateur">
            
            <CreationUtilisateur />
          </Route>
          <Route path="/ListerBatiments">
            
            <ListerBatiments />
          </Route>
          <Route path="/CreationBatiment">
            
            <CreationBatiment />
          </Route>
        
          <Route path="/ModificationBatiment/:id">
            <ModificationBatiment />
          </Route>
          <Route path="/CreationSalle">
            
            <CreationSalle />
          </Route>

          <Route path ="/CreationReservation">
            <CreationReservation />
          </Route>

          <Route path ="/ModificationReservation/:id">
            <ModificationReservation />
          </Route>
          <Route path="/CreationTypeSalle">
            
            <CreationTypeSalle />
          </Route>
          <Route path="/ListerUtilisateur">
            
            <ListerUtilisateur />
          </Route>
           <Route  path="/voirReservation/:id">

            <VoirReservation />
          </Route>
          <Route exact path="/ListerReservations">

            <ListerReservations />
          </Route>

          <Route exact path="/voirReservation/:id">

            <VoirReservation />
          </Route>
          <Route path="/ModificationUtilisateur/:id">
            <ModificationUtilisateur />
          </Route>
          <Route path="/ModificationTypeSalle/:id">
            <ModificationTypeSalle />
          </Route>
          <Route exact path="/VoirUser/:id">
            <VoirUser />
          </Route>
          
          <Route path="/ListerTypeSalle">
            
            <ListerTypeSalle />
          </Route>
          <Route path="/CreationTypeMateriel">
            
            <CreationTypeMateriel />
          </Route>
          <Route path="/ListerTypeMateriel">
            
            <ListerTypeMateriel />
          </Route>
          <Route path="/ListerSalle">
            <ListerSalle />
          </Route>
          <Route exact path="/VoirSalle/:id">
            <VoirSalle />
          </Route>
          <Route exact path="/ModificationSalle/:id">
            <ModificationSalle />
          </Route>
          <Route path="/ModifSalle/:id">
            <ModifSalle />
          </Route>
          <Route path="/ModificationTypeMateriel/:id">
            
            <ModificationTypeMateriel />
          </Route>
          <Route path="/">
            <Accueil />
          </Route>
        </Switch>
        </Router>        
      </div>
      </ToastProvider>
    );
  }
}

export default App;
